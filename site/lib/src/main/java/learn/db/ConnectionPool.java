package learn.db;

import java.sql.Connection;
import java.sql.SQLException;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public enum ConnectionPool {
	INSTANCE;
	
	private HikariDataSource ds;
	private ConnectionPool(){
		HikariConfig config = new HikariConfig();
		config.setDriverClassName("org.h2.Driver");
		config.setJdbcUrl("jdbc:h2:./dbStore");
		config.setUsername("learn");
		config.setPassword("learn");
		ds = new HikariDataSource(config);
	}
	public Connection getConnection() throws SQLException{
		return ds.getConnection();
	}
}
