package learn.service;

import java.util.List;

import learn.dto.TestDto;

public interface TestService {

	public List<TestDto> getTestList();
}
