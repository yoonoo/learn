package learn.service.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import learn.db.ConnectionPool;
import learn.dto.TestDto;
import learn.service.TestService;

public class JdbcV2TestService implements TestService{
	
	public List<TestDto> getTestList(){
		try(Connection conn = ConnectionPool.INSTANCE.getConnection();
			PreparedStatement psmt = conn.prepareStatement("select * from INFORMATION_SCHEMA.TYPE_INFO");
			ResultSet rs = psmt.executeQuery();){
			
			ArrayList<TestDto> testList = new ArrayList<>();
			while(rs.next()){
				TestDto testDto = new TestDto();
				testDto.setTypeName(rs.getString("type_name"));
				testDto.setDataType(rs.getInt("data_type"));
				testList.add(testDto);
			}
			
			return testList;
			
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		
		return null;
	}
}
