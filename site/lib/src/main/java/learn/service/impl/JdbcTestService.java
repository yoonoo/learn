package learn.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import learn.dto.TestDto;
import learn.service.TestService;

public class JdbcTestService implements TestService{
	
	public List<TestDto> getTestList(){
		Connection conn = null;
		PreparedStatement psmt = null;
		ResultSet rs = null;
		try {
			Class.forName("org.h2.Driver");
			conn = DriverManager.getConnection("jdbc:h2:./dbStore", "learn", "learn");
			psmt = conn.prepareStatement("select * from INFORMATION_SCHEMA.TYPE_INFO ");
			rs = psmt.executeQuery();
			
			ArrayList<TestDto> testList = new ArrayList<>();
			while(rs.next()){
				TestDto testDto = new TestDto();
				testDto.setTypeName(rs.getString("type_name"));
				testDto.setDataType(rs.getInt("data_type"));
				testList.add(testDto);
			}
			
			return testList;
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally{
			if(rs != null){try{rs.close();}catch(Exception e){}}
			if(psmt != null){try{psmt.close();}catch(Exception e){}}
			if(conn != null){try{conn.close();}catch(Exception e){}}
		}
		return null;
	}
}
