package learn.service.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import learn.db.mybatis.SessionFactory;
import learn.dto.TestDto;
import learn.service.TestService;

public class MybatisTestService implements TestService{
	
	public List<TestDto> getTestList(){
		try(SqlSession session = SessionFactory.INSTANCE.getSession()){
			return session.selectList("learn.test.select");
		}
	}
}
